package com.kneckcorp.kneckbot.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import discord4j.voice.AudioProvider;

public class GuildMusicManager {
    /**
     * Audio player for the guild.
     */
    public AudioPlayer player;

    /**
     * Track scheduler for the player.
     */
    public TrackScheduler scheduler;
    public AudioPlayerManager playerManager;
    public AudioProvider provider;

    /**
     * Creates a player and a track scheduler.
     * @param manager Audio player manager to use for creating the player.
     */
    public GuildMusicManager(AudioPlayerManager manager) {
        this.playerManager = manager;
        this.player = manager.createPlayer();
        this.scheduler = new TrackScheduler(player);
        this.provider = new LavaplayerAudioProvider(player);
    }
}
