package com.kneckcorp.kneckbot.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackScheduler implements AudioLoadResultHandler {

    private AudioPlayer player;

    private static Logger LOG = LoggerFactory.getLogger(TrackScheduler.class);

    public TrackScheduler(AudioPlayer player) {
        this.player = player;
    }

    @Override
    public void trackLoaded(AudioTrack track) {
        LOG.info("Playing track " + track.getIdentifier());
        player.playTrack(track);
    }

    @Override
    public void playlistLoaded(AudioPlaylist playlist) {

    }

    @Override
    public void noMatches() {
        LOG.info("No matches found!");
    }

    @Override
    public void loadFailed(FriendlyException exception) {
        LOG.info("Couldn't load file " + exception.toString() + "!");
    }
}