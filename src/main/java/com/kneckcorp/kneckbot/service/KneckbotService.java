package com.kneckcorp.kneckbot.service;
import org.springframework.stereotype.Service;

@Service
public interface KneckbotService {

    void startBot();
}
