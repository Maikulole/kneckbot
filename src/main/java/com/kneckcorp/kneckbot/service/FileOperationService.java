package com.kneckcorp.kneckbot.service;

import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.InputStream;

@Service
public interface FileOperationService {

    InputStream getImageInputStream(String path) throws FileNotFoundException;
    String getRandomFilePathFromFolder(String path);

    String getRandomFilePathFromNestedFolder(String path);

    String getRandomFilePathFromNestedFolder(String topLevelFolder, String path);
    String stripFileName(String filePath);
}
