package com.kneckcorp.kneckbot.service.impl;

import com.kneckcorp.kneckbot.commands.util.KneckbotCommandConnection;
import com.kneckcorp.kneckbot.properties.LoginProperties;
import com.kneckcorp.kneckbot.service.KneckbotService;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KneckbotServiceImpl implements KneckbotService {

    private static final Logger LOG = LoggerFactory.getLogger(KneckbotServiceImpl.class);

    @Autowired
    private LoginProperties loginProperties;
    @Autowired
    private KneckbotCommandConnection kneckbotCommandConnection;

    @Override
    public void startBot() {
        final DiscordClient client = new DiscordClientBuilder(loginProperties.getDiscordToken()).build();
        client.getEventDispatcher().on(ReadyEvent.class)
                .subscribe(ready -> LOG.info("Logged in as {}", new Object[]{ ready.getSelf().getUsername() }));
        kneckbotCommandConnection.setBootstrapper();
        kneckbotCommandConnection.setupCommands(client);
        client.login().block();
    }
}
