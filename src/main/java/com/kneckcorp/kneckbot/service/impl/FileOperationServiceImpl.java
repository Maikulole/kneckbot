package com.kneckcorp.kneckbot.service.impl;

import com.kneckcorp.kneckbot.service.FileOperationService;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;

@Component
public class FileOperationServiceImpl implements FileOperationService {
    private Random rand = new Random();

    @Override
    public InputStream getImageInputStream(String path) throws FileNotFoundException {
        return new FileInputStream(path);
    }

    @Override
    public String getRandomFilePathFromFolder(String path) {
        final File dir = new File(path);
        File[] files = dir.listFiles();
        File imageFile =  Objects.requireNonNull(files)[rand.nextInt(files.length)];
        return imageFile.getAbsolutePath();
    }

    @Override
    public String getRandomFilePathFromNestedFolder(String path) {
        final File dir = new File(path);
        File[] files = dir.listFiles();
        File heroFolder = Objects.requireNonNull(files)[rand.nextInt(files.length)];
        return getRandomFilePathFromFolder(heroFolder.getAbsolutePath());
    }

    @Override
    public String getRandomFilePathFromNestedFolder(String topLevelFolder, String path) {
        final File dir = new File(path);
        File[] files = dir.listFiles();
        String finalPath = "";
        for (File file : Objects.requireNonNull(files)) {
            if (file.getName().equalsIgnoreCase(topLevelFolder)) {
                finalPath = file.getAbsolutePath();
            }
        }
        return getRandomFilePathFromFolder(finalPath);
    }

    @Override
    public String stripFileName(String filePath) {
        return filePath.substring(filePath.lastIndexOf("\\")+1);
    }
}
