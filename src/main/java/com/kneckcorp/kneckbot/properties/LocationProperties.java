package com.kneckcorp.kneckbot.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:config/kneckbot.properties")
@ConfigurationProperties("location")
public class LocationProperties {

    private String pathToMoneyBoy;
    private String pathToLebron;
    private String pathToMicha;
    private String pathToDoot;
    private String pathToDoto;

    public String getPathToDoto() {
        return pathToDoto;
    }

    public void setPathToDoto(String pathToDoto) {
        this.pathToDoto = pathToDoto;
    }

    public String getPathToDoot() {
        return pathToDoot;
    }

    public void setPathToDoot(String pathToDoot) {
        this.pathToDoot = pathToDoot;
    }

    public String getPathToMoneyBoy() {
        return pathToMoneyBoy;
    }

    public void setPathToMoneyBoy(String pathToMoneyBoy) {
        this.pathToMoneyBoy = pathToMoneyBoy;
    }

    public String getPathToLebron() {
        return pathToLebron;
    }

    public void setPathToLebron(String pathToLebron) {
        this.pathToLebron = pathToLebron;
    }

    public String getPathToMicha() {
        return pathToMicha;
    }

    public void setPathToMicha(String pathToMicha) {
        this.pathToMicha = pathToMicha;
    }
}
