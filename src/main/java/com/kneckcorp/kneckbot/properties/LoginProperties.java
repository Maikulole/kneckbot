package com.kneckcorp.kneckbot.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:config/kneckbot.properties")
@ConfigurationProperties("login")
public class LoginProperties {

    private String discordToken;

    public String getDiscordToken() {
        return discordToken;
    }

    public void setDiscordToken(String discordToken) {
        this.discordToken = discordToken;
    }
}
