package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.audio.GuildMusicManager;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class StopCommand implements Command {

    private GuildMusicManager guildMusicManager;

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        return Mono.justOrEmpty(event.getClient())
                .doOnNext(e -> guildMusicManager.player.stopTrack())
                .then();
    }

    public void setGuildMusicManager(GuildMusicManager guildMusicManager) {
        this.guildMusicManager = guildMusicManager;
    }
}
