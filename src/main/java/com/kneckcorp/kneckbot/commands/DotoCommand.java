package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.audio.GuildMusicManager;
import com.kneckcorp.kneckbot.audio.LavaplayerAudioProvider;
import com.kneckcorp.kneckbot.audio.TrackScheduler;
import com.kneckcorp.kneckbot.properties.LocationProperties;
import com.kneckcorp.kneckbot.service.FileOperationService;
import com.kneckcorp.kneckbot.service.impl.FileOperationServiceImpl;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.voice.AudioProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DotoCommand implements Command {

    @Autowired
    private LocationProperties locationProperties;
    private GuildMusicManager guildMusicManager;
    private static Logger LOG = LoggerFactory.getLogger(MoneyboyCommand.class);

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        if (event.getMessage().getContent().isPresent()) {
            String[] msgArray = event.getMessage().getContent().get().split(" ");
            String filePath;
            FileOperationService fileOperationService = new FileOperationServiceImpl();

            if (msgArray.length > 1) {
                filePath = fileOperationService.getRandomFilePathFromNestedFolder(msgArray[1], locationProperties.getPathToDoto());
            } else {
                filePath = fileOperationService.getRandomFilePathFromNestedFolder(locationProperties.getPathToDoto());
            }

            Mono.justOrEmpty(event.getMember())
                    .flatMap(Member::getVoiceState)
                    .flatMap(VoiceState::getChannel)
                    .flatMap(channel -> channel.join(spec -> {
                        spec.setProvider(guildMusicManager.provider);
                    })).subscribe();

            return Mono.justOrEmpty(event.getMessage().getContent())
                    .doOnNext(command -> guildMusicManager.playerManager.loadItem(filePath, guildMusicManager.scheduler))
                    .then();
        }
        return Mono.justOrEmpty(event.getMessage().getContent())
                .doOnNext(command -> LOG.info("Message broken on Doto Command!"))
                .then();
    }

    public void setGuildMusicManager(GuildMusicManager guildMusicManager) {
        this.guildMusicManager = guildMusicManager;
    }
}
