package com.kneckcorp.kneckbot.commands;

import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class HelpCommand implements Command {

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {

        Map<String, String> commands = new HashMap<>();
        commands.put("!help", "Lists all available commands and perhaps some more information");
        commands.put("!info", "Posts some info about the bot");
        commands.put("!lebron", "Funny pictures of Lebron");
        commands.put("!micha", "Funny pictures of Micha");
        commands.put("!scurr, !burr", "Funny pictures of");
        commands.put("!play", "Play some music from Youtube, Soundcloud and so on");
        commands.put("!stop", "Stop that shit");
        commands.put("!random val...", "Returns a random value from the list");
        commands.put("!doot", "CALCIUM");
        commands.put("!doto", "Play a random voice line from DotA 2");

        return Mono.just(commandsToString(commands))
                .zipWith(event.getMessage().getChannel())
                .flatMap(tuple -> tuple.getT2().createMessage(messageSpec -> {
                    messageSpec.setEmbed(embedCreateSpec -> {
                        for (Map.Entry<String, String> entry : commands.entrySet()) {
                            embedCreateSpec.addField(entry.getKey(), entry.getValue(), false);
                        }
                        embedCreateSpec.setThumbnail("https://assets.gitlab-static.net/uploads/-/system/project/avatar/13483385/Unbenannt.png?width=64");
                        embedCreateSpec.setColor(Color.BLACK);
                    });
                }))
                .then();
    }

    private String commandsToString(Map<String, String> commands) {
        return commands.entrySet().stream()
                .map(x -> x.getKey() + " - " + x.getValue())
                .collect(Collectors.joining("\n"));
    }
}
