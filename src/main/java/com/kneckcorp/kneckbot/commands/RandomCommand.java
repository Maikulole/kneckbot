package com.kneckcorp.kneckbot.commands;

import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class RandomCommand implements Command {

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        return Mono.justOrEmpty(event)
                .zipWith(event.getMessage().getChannel())
                .flatMap(channel -> channel.getT2().createMessage(messageCreateSpec -> {
                    if (event.getMessage().getContent().isPresent()) {
                        String[] content = event.getMessage().getContent().get().split(" ");
                        if (content.length > 1) {
                            int randomNum = ThreadLocalRandom.current().nextInt(1, content.length);
                            messageCreateSpec.setContent("Random: " + content[randomNum].substring(0, 1).toUpperCase() + content[randomNum].substring(1));
                        } else {
                            messageCreateSpec.setContent("Random needs at least 1 parameter!");
                        }
                    } else {
                        messageCreateSpec.setContent("No Message found apparently :(");
                    }
                }).then());
    }
}
