package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.audio.GuildMusicManager;
import com.kneckcorp.kneckbot.audio.LavaplayerAudioProvider;
import com.kneckcorp.kneckbot.audio.TrackScheduler;
import com.kneckcorp.kneckbot.properties.LocationProperties;
import com.kneckcorp.kneckbot.service.FileOperationService;
import com.kneckcorp.kneckbot.service.impl.FileOperationServiceImpl;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.voice.AudioProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DootCommand implements Command {

    @Autowired
    private LocationProperties locationProperties;
    private GuildMusicManager guildMusicManager;
    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        FileOperationService fileOperationService = new FileOperationServiceImpl();
        String filePath = fileOperationService.getRandomFilePathFromFolder(locationProperties.getPathToDoot());

        Mono.justOrEmpty(event.getMember())
                .flatMap(Member::getVoiceState)
                .flatMap(VoiceState::getChannel)
                .flatMap(channel -> channel.join(spec -> {
                    spec.setProvider(guildMusicManager.provider);
                })).subscribe();

        Mono.just(event)
                .zipWith(event.getMessage().getChannel())
                .flatMap(tuple -> tuple.getT2().createMessage(messageSpec -> {
                    messageSpec.setContent("https://i.imgur.com/wdYNADu.gif");
                })).subscribe();

        return Mono.justOrEmpty(event.getMessage().getContent())
                .doOnNext(command -> guildMusicManager.playerManager.loadItem(filePath, guildMusicManager.scheduler))
                .then();
    }

    public void setGuildMusicManager(GuildMusicManager guildMusicManager) {
        this.guildMusicManager = guildMusicManager;
    }
}
