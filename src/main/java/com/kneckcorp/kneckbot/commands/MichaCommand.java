package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.properties.LocationProperties;
import com.kneckcorp.kneckbot.service.FileOperationService;
import com.kneckcorp.kneckbot.service.impl.FileOperationServiceImpl;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.InputStream;

@Component
public class MichaCommand implements Command {

    @Autowired
    private LocationProperties locationProperties;

    private static Logger LOG = LoggerFactory.getLogger(MichaCommand.class);

    @Override
    @SuppressWarnings("Duplicates")
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        FileOperationService fileOperationService = new FileOperationServiceImpl();
        InputStream micha = null;
        String filePath = fileOperationService.getRandomFilePathFromFolder(locationProperties.getPathToMicha());
        String fileName = fileOperationService.stripFileName(filePath);

        try {
            micha = fileOperationService.getImageInputStream(filePath);
        } catch (FileNotFoundException e) {
            LOG.error(e.toString());
        }

        InputStream finalMicha = micha;

        return Mono.just(event)
                .zipWith(event.getMessage().getChannel())
                .flatMap(tuple -> tuple.getT2().createMessage(messageSpec -> {
                    messageSpec.addFile(fileName, finalMicha);
                })).then();
    }

}
