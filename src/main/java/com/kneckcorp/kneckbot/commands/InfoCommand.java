package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.commands.util.KneckbotCommandProvider;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.awt.*;
import java.util.concurrent.TimeUnit;

@Component
public class InfoCommand implements Command {

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {
        return Mono.just(event)
                .flatMap(channel -> event.getMessage().getChannel())
                .flatMap(messageChannel -> messageChannel.createMessage(messageCreateSpec -> {
                    messageCreateSpec.setEmbed(embedCreateSpec -> {
                        String uptime = timeConversion((int) TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - KneckbotCommandProvider.getStartTime()));
                        embedCreateSpec.addField("Kneckbot", "Now even better!", false);
                        embedCreateSpec.addField("Uptime", uptime, false);
                        embedCreateSpec.addField("Source", "[Gitlab](https://gitlab.com/Maikulole/KneckBot)", false);
                        embedCreateSpec.setImage("https://i.imgur.com/or6HiCo.png");
                        embedCreateSpec.setThumbnail("https://assets.gitlab-static.net/uploads/-/system/project/avatar/13483385/Unbenannt.png?width=64");
                        embedCreateSpec.setFooter("Version 0.2", "");
                        embedCreateSpec.setColor(Color.BLACK);
                    });
                }))
                .then();
    }

    /**
     * Converts seconds to days, hours, minutes and seconds
     */
    private String timeConversion(int seconds) {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
        return day + " days " + hours + " hours " + minute + " minutes " + second + " seconds";
    }

}
