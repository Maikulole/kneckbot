package com.kneckcorp.kneckbot.commands.util;

import com.kneckcorp.kneckbot.properties.BotProperties;
import discord4j.command.CommandBootstrapper;
import discord4j.command.util.AbstractCommandDispatcher;
import discord4j.core.DiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class KneckbotCommandConnection extends AbstractCommandDispatcher{
    @Autowired
    private KneckbotCommandProvider kneckbotCommandProvider;
    @Autowired
    private BotProperties botProperties;

    private CommandBootstrapper bootstrapper;
    public KneckbotCommandConnection(){

    }

    @Override
    protected Publisher<String> getPrefixes(MessageCreateEvent event) {
        return Mono.just(botProperties.getPrefix());
    }

    public void setupCommands(DiscordClient client) {
        bootstrapper.addProvider(kneckbotCommandProvider);
        bootstrapper.attach(client).subscribe();
    }

    public void setBootstrapper() {
        this.bootstrapper = new CommandBootstrapper(this);
    }
}
