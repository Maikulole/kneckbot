package com.kneckcorp.kneckbot.commands.util;

import com.kneckcorp.kneckbot.audio.GuildMusicManager;
import com.kneckcorp.kneckbot.commands.*;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer;
import discord4j.command.CommandProvider;
import discord4j.command.ProviderContext;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.util.Snowflake;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@Configurable
public class KneckbotCommandProvider implements CommandProvider {

    @Autowired
    private MoneyboyCommand moneyboyCommand;

    @Autowired
    private HelpCommand helpCommand;

    @Autowired
    private PlayCommand playCommand;

    @Autowired
    private StopCommand stopCommand;

    @Autowired
    private InfoCommand infoCommand;

    @Autowired
    private LebronCommand lebronCommand;

    @Autowired
    private MichaCommand michaCommand;

    @Autowired
    private RandomCommand randomCommand;

    @Autowired
    private DootCommand dootCommand;

    @Autowired
    private DotoCommand dotoCommand;

    private static long currentTime = System.nanoTime();

    private AudioPlayerManager playerManager;

    private HashMap<Long, GuildMusicManager> guildMusicManagerMap = new HashMap<>();

    public KneckbotCommandProvider() {
        playerManager = new DefaultAudioPlayerManager();
        playerManager.getConfiguration().setFrameBufferFactory(NonAllocatingAudioFrameBuffer::new);
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
    }

    void tryRegisterNewMusicManager(Long guildId){

        if(!this.guildMusicManagerMap.containsKey(guildId))
            this.guildMusicManagerMap.put(guildId, new GuildMusicManager(playerManager));
    }

    @Override
    public Publisher<ProviderContext> provide(MessageCreateEvent context, String commandName, int commandStartIndex, int commandEndIndex) {
        tryRegisterNewMusicManager(context.getGuildId().get().asLong());
        return Mono.just(commandName)
                .map(cmd -> {
                    if (cmd.equalsIgnoreCase("help")) {
                        return ProviderContext.of(helpCommand);
                    } else if (cmd.equalsIgnoreCase("info")) {
                        return ProviderContext.of(infoCommand);
                    } else if (cmd.equalsIgnoreCase("burr") || cmd.equalsIgnoreCase("scurr")) {
                        return ProviderContext.of(moneyboyCommand);
                    } else if (cmd.equalsIgnoreCase("lebron")) {
                        return ProviderContext.of(lebronCommand);
                    } else if (cmd.equalsIgnoreCase("micha")) {
                        return ProviderContext.of(michaCommand);
                    } else if (cmd.equalsIgnoreCase("play")) {
                        playCommand.setGuildMusicManager(this.guildMusicManagerMap.get(context.getGuildId().get().asLong()));
                        return ProviderContext.of(playCommand);
                    } else if (cmd.equalsIgnoreCase("stop")) {
                        stopCommand.setGuildMusicManager(this.guildMusicManagerMap.get(context.getGuildId().get().asLong()));
                        return ProviderContext.of(stopCommand);
                    } else if (cmd.equalsIgnoreCase("random")) {
                        return ProviderContext.of(randomCommand);
                    } else if (cmd.equalsIgnoreCase("doot")) {
                        dootCommand.setGuildMusicManager(this.guildMusicManagerMap.get(context.getGuildId().get().asLong()));
                        return ProviderContext.of(dootCommand);
                    } else if (cmd.equalsIgnoreCase("doto")) {
                        dotoCommand.setGuildMusicManager(this.guildMusicManagerMap.get(context.getGuildId().get().asLong()));
                        return ProviderContext.of(dotoCommand);
                    } else {
                        return null;
                    }
                }).flux();
    }

    public static long getStartTime() {
        return currentTime;
    }
}
