package com.kneckcorp.kneckbot.commands;

import com.kneckcorp.kneckbot.audio.GuildMusicManager;
import com.kneckcorp.kneckbot.audio.LavaplayerAudioProvider;
import com.kneckcorp.kneckbot.audio.TrackScheduler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import discord4j.command.Command;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.voice.AudioProvider;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@Component
public class PlayCommand implements Command {

    private GuildMusicManager guildMusicManager;

    @Override
    public Mono<Void> execute(MessageCreateEvent event, Object context) {

        Mono.justOrEmpty(event.getMember())
                .flatMap(Member::getVoiceState)
                .flatMap(VoiceState::getChannel)
                .flatMap(channel -> channel.join(spec -> {
                    spec.setProvider(guildMusicManager.provider);
                })).subscribe();

        return Mono.justOrEmpty(event.getMessage().getContent())
                .map(content -> Arrays.asList(content.split(" ")))
                .doOnNext(command -> guildMusicManager.playerManager.loadItem(command.get(1), guildMusicManager.scheduler))
                .then();
    }
    public void setGuildMusicManager(GuildMusicManager guildMusicManager) {
        this.guildMusicManager = guildMusicManager;
    }
}
