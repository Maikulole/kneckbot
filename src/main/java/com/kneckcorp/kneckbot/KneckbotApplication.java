package com.kneckcorp.kneckbot;

import com.kneckcorp.kneckbot.service.KneckbotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@EnableConfigurationProperties
@SpringBootApplication
@Component
public class KneckbotApplication implements CommandLineRunner {
    @Qualifier("kneckbotServiceImpl")
    @Autowired
    private KneckbotService kneckBotService;

    public static void main(String[] args) {
        SpringApplication.run(KneckbotApplication.class, args);
    }

    @Override
    public void run(String... args) {
        kneckBotService.startBot();
    }
}
