FROM openjdk:8-jdk-alpine
LABEL maintainer="Kneckcorp"
VOLUME /tmp
EXPOSE 8081
ARG JAR_FILE=build/libs/*.jar
ADD ${JAR_FILE} kneckbot.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/kneckbot.jar"]